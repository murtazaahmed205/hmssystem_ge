﻿using HMSSystem_GE.Models;
using HMSSystem_GE.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.InteropServices;

namespace HMSSystem_GE.Controllers
{
    [ApiController]
    [Route("api")]
    public class PatientController : ControllerBase
    {

        private readonly IPatientService _patientService;
        private readonly ILogger<PatientController> logger;
        ResponseModel _responseModel = new ResponseModel();
        public PatientController(ILogger<PatientController> logger,IPatientService patientService)
        {
            _patientService = patientService;
            this.logger = logger;
        }

        [HttpGet("get-patients")]
        public async Task<IActionResult> GetAllPatientsAsync() 
        {
            try
            {
                var patientsAndDoctors = await _patientService.GetAllPatientAsync();
                _responseModel.success = true;
                _responseModel.message = "record found.";
                _responseModel.data = patientsAndDoctors;
                logger.LogDebug("API: get-patients", _responseModel.data);
                return Ok(_responseModel);
            }
            catch (Exception ex)
            {
                _responseModel.success = false;
                _responseModel.message = ex.Message;
                logger.LogError("API: get-patients :" + _responseModel.message);
                return StatusCode(500, _responseModel);
            }
        
        }

        [HttpGet("get-patientsbyid/{id}")]
        public async Task<IActionResult> GetPatientByIdAsync(int id)
        {
            try
            {
                var success = await _patientService.GetPatientByIdAsync(id);
                if (success != null) 
                {
                    _responseModel.success = true;
                    _responseModel.message = "record delete successfully.";
                    _responseModel.data = success;
                    logger.LogDebug("API: delete-patientsbyid :", _responseModel.data);
                    return Ok(_responseModel);
                }
                else {
                    _responseModel.success = false;
                    _responseModel.message = "no record found.";
                    logger.LogError("API: delete-patientsbyid :" + _responseModel.message);
                    return StatusCode(500, _responseModel);
                }
            }
            catch (Exception ex)
            {
                _responseModel.success = false;
                _responseModel.message = ex.Message;
                logger.LogError("API: delete-patientsbyid :" + _responseModel.message);
                return StatusCode(500, _responseModel);
            }

        }

        [HttpPost("delete-patient/{id}")]
        public async Task<IActionResult> DeletePatientByIdAsync(int id)
        {
            try
            {
                int success = await _patientService.DeletePatientAsync(id);
                if (success > 0)
                {
                    _responseModel.success = true;
                    _responseModel.message = "record delete successfully.";
                    logger.LogDebug("API: delete-patient :", _responseModel.data);
                    return Ok(_responseModel);
                }
                else {
                    _responseModel.success = false;
                    _responseModel.message = "Failed to delete record.";
                    logger.LogError("API: delete-patient :" + _responseModel.message);
                    return StatusCode(500, _responseModel);
                }
            }
            catch (Exception ex)
            {
                _responseModel.success = false;
                _responseModel.message = "Failed to delete record.";
                logger.LogError("API: delete-patient :" + _responseModel.message);
                return StatusCode(500, _responseModel);
            }

        }

        [HttpPost("save-patient")]
        public async Task<IActionResult> SavePatientAsync(PatientAndTheriDetails _data)
        {
            try
            {
                int Id = await _patientService.SavePatientAsync(_data.patientData);
                if (Id > 0)
                {
                    _data.patientDetailsData.FkPatientId = Id;
                    await _patientService.SavePatientDetailAsync(_data.patientDetailsData);
                    _responseModel.success = true;
                    _responseModel.message = "Record saved successfully.";
                    _responseModel.data = _data;
                    logger.LogDebug("API: save-patient", _data);
                    return Ok(_responseModel);
                }
                else
                {
                    _responseModel.success = false;
                    _responseModel.message = "Failed to save record.";
                    logger.LogError("API: save-patient :" + _responseModel.message);
                    return StatusCode(500, _responseModel);
                }
            }
            catch (Exception ex)
            {
                _responseModel.success = false;
                _responseModel.message = ex.Message;
                logger.LogError("API: save-patient :" + ex.Message);
                return StatusCode(500, _responseModel);
            }
        }

        [HttpPost("update-patient")]
        public async Task<IActionResult> UpdatePatientAsync(Patient _data)
        {
            try
            {
                bool success = await _patientService.UpdatePatientAsync(_data);
                if (success)
                {
                    _responseModel.success = true;
                    _responseModel.message = "Record update successfully.";
                    _responseModel.data = _data;
                    logger.LogDebug("API: update-patient", _data);
                    return Ok(_responseModel);
                }
                else
                {
                    _responseModel.success = false;
                    _responseModel.message = "Failed to update record.";
                    logger.LogError("API: update-patient :" + _responseModel.message);
                    return StatusCode(500, _responseModel);
                }
            }
            catch (Exception ex)
            {
                _responseModel.success = false;
                _responseModel.message = ex.Message;
                logger.LogError("API: update-patient :" + ex.Message);
                return StatusCode(500, _responseModel);
            }
        }

        [HttpGet("get-doctors")]
        public async Task<IActionResult> GetAllDoctors()
        {
            try
            {
                var result = await _patientService.GetAllDoctorsAsync();
                if (result.Count > 0)
                {
                    _responseModel.success = true;
                    _responseModel.message = "record found.";
                    _responseModel.data = result;
                    logger.LogDebug("API: get-doctors", _responseModel.data);
                    return Ok(_responseModel);
                }
                else
                {
                    _responseModel.success = false;
                    _responseModel.message = "no record found.";
                    _responseModel.data = result;
                    logger.LogError("API: get-doctors", _responseModel.message);
                    return StatusCode(500, _responseModel);
                }
            }
            catch (Exception ex)
            {
                _responseModel.success = false;
                _responseModel.message = ex.Message;
                logger.LogError("API: get-doctors :" + _responseModel.message);
                return StatusCode(500, _responseModel);
            }

        }

    }
}
