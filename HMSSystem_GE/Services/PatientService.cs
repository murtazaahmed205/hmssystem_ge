﻿using HMSSystem_GE.Models;
using Microsoft.EntityFrameworkCore;

namespace HMSSystem_GE.Services
{
    public class PatientService : IPatientService
    {
        private readonly HmssystemContext _Context;
        public PatientService()
        {   
            _Context = new HmssystemContext();
        }
        public async Task<int> DeletePatientAsync(object Id)
        {
            var patient = await _Context.Patients.FindAsync(Id);
            if (patient != null)
            {
                _Context.Patients.Remove(patient);
                return await _Context.SaveChangesAsync();
            }
            else 
            {
                return 0;
            }
        }

        public async Task<List<Patient>> GetAllPatientAsync()
        {
            return await _Context.Patients.ToListAsync();
        }

        public async Task<Patient> GetPatientByIdAsync(object Id)
        {
            var patient = await _Context.Patients.FindAsync(Id);
            return patient;
        }

        public async Task<int> SavePatientAsync(Patient data)
        {
            try
            {
                await _Context.Patients.AddAsync(data);
                await _Context.SaveChangesAsync();
                int newPatientId = data.PkId;
                return newPatientId;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public async Task<bool> SavePatientDetailAsync(PatientsDetail data) 
        {
            try
            {
                await _Context.PatientsDetails.AddAsync(data);
                await _Context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public async Task<bool> UpdatePatientAsync(Patient data) 
        {
            try
            {
                _Context.Entry(data).State = EntityState.Modified;
                await _Context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public async Task<List<Doctor>> GetAllDoctorsAsync()
        {
            return await _Context.Doctors.ToListAsync();
        }
        //public async Task<IEnumerable<PatientDoctorDto>> GetAllPatientsAndTheirDoctorsAsync()
        //{
        //    //var patientsAndDoctors = await (from patient in _Context.Patients
        //    //                                join doctor in _Context.Doctors
        //    //                                on patient.FkDoctorId equals doctor.PkId
        //    //                                select new PatientDoctorDto { Patient = patient, Doctor = doctor }).ToListAsync();

        //    //return patientsAndDoctors;
        //}
    }
}
