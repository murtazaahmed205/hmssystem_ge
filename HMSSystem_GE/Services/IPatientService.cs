﻿using HMSSystem_GE.Models;

namespace HMSSystem_GE.Services
{
    public interface IPatientService
    {
        Task<int> SavePatientAsync(Patient data);
        Task<bool> UpdatePatientAsync(Patient data);
        Task<int> DeletePatientAsync(object Id);
        Task<Patient> GetPatientByIdAsync(object Id);
        Task<List<Patient>> GetAllPatientAsync();
        Task<List<Doctor>> GetAllDoctorsAsync();
        //Task<IEnumerable<PatientDoctorDto>> GetAllPatientsAndTheirDoctorsAsync();
        Task<bool> SavePatientDetailAsync(PatientsDetail data);
    }
}
