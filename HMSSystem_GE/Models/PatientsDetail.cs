﻿using System;
using System.Collections.Generic;

namespace HMSSystem_GE.Models;

public partial class PatientsDetail
{
    public int PkId { get; set; }

    public int FkPatientId { get; set; }

    public DateTime VisitDate { get; set; }

    public string DiseasesDetails { get; set; } = null!;

    public int FkDoctorId { get; set; }

    public int FkDiseasesId { get; set; }
}
