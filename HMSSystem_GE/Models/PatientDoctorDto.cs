﻿namespace HMSSystem_GE.Models
{
    public class PatientDoctorDto
    {
        public Patient Patient { get; set; }
        public Doctor Doctor { get; set; }
    }
}
