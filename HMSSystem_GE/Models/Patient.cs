﻿using System;
using System.Collections.Generic;

namespace HMSSystem_GE.Models;

public partial class Patient
{
    public int PkId { get; set; }

    public string Name { get; set; } = null!;

    public int Age { get; set; }

    public string Gender { get; set; } = null!;

    public string Phone { get; set; } = null!;

    public string Address { get; set; } = null!;

    public bool Status { get; set; }
}
