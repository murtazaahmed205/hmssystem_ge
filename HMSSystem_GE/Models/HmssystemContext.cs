﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace HMSSystem_GE.Models;

public partial class HmssystemContext : DbContext
{
    public HmssystemContext()
    {
    }

    public HmssystemContext(DbContextOptions<HmssystemContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Disease> Diseases { get; set; }

    public virtual DbSet<Doctor> Doctors { get; set; }

    public virtual DbSet<Patient> Patients { get; set; }

    public virtual DbSet<PatientsDetail> PatientsDetails { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=DESKTOP-VEPGOMP;Initial Catalog=HMSSystem;Trusted_Connection=True;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Disease>(entity =>
        {
            entity.HasKey(e => e.PkId);

            entity.Property(e => e.PkId).HasColumnName("Pk_Id");
            entity.Property(e => e.DiseasesName)
                .HasMaxLength(50)
                .HasColumnName("Diseases_Name");
        });

        modelBuilder.Entity<Doctor>(entity =>
        {
            entity.HasKey(e => e.PkId);

            entity.Property(e => e.PkId).HasColumnName("Pk_Id");
            entity.Property(e => e.DoctorName)
                .HasMaxLength(100)
                .HasColumnName("Doctor_name");
        });

        modelBuilder.Entity<Patient>(entity =>
        {
            entity.HasKey(e => e.PkId);

            entity.Property(e => e.PkId).HasColumnName("Pk_Id");
            entity.Property(e => e.Address).HasColumnType("text");
            entity.Property(e => e.Gender).HasMaxLength(50);
            entity.Property(e => e.Name).HasMaxLength(100);
            entity.Property(e => e.Phone).HasMaxLength(50);
        });

        modelBuilder.Entity<PatientsDetail>(entity =>
        {
            entity.HasKey(e => e.PkId);

            entity.Property(e => e.PkId).HasColumnName("Pk_Id");
            entity.Property(e => e.DiseasesDetails)
                .HasColumnType("text")
                .HasColumnName("Diseases_details");
            entity.Property(e => e.FkDiseasesId).HasColumnName("Fk_DiseasesId");
            entity.Property(e => e.FkDoctorId).HasColumnName("Fk_DoctorId");
            entity.Property(e => e.FkPatientId).HasColumnName("Fk_Patient_Id");
            entity.Property(e => e.VisitDate)
                .HasColumnType("datetime")
                .HasColumnName("Visit_Date");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
