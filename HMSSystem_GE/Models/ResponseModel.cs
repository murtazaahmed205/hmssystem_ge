﻿namespace HMSSystem_GE.Models
{
    public class ResponseModel
    {
        public bool success { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
}
